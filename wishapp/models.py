from __future__ import unicode_literals
from django.db import models
import re

class UserManager(models.Manager):
    def create_validator(self, reqPOST):
        errors = {}
        EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')
        if len(reqPOST['first_name']) < 2:
            errors['first_name'] = "first name must be at least 2 chars"
        if len(reqPOST['last_name']) < 2:
            errors['last_name'] = "first name must be at least 2 chars"
        if not EMAIL_REGEX.match(reqPOST['email']):
            errors['email'] = "email must be valid format"
        if len(reqPOST['password']) < 6:
            errors['password'] = 'password must be at least 8 chars'
        if reqPOST['password'] != reqPOST['confirm_password']:
            errors['confirm_password'] = "passwords do not match"
        return errors


class User(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()


class WishManager(models.Manager):
    def create_validator(self, reqPOST):
        errors = {}
        if len(reqPOST['wish_name']) < 2:
            errors['wish_name'] = "wish name is too short"
        if len(reqPOST['description']) < 2:
            errors['description'] = "description is too short"
        wishes_with_same_name = Wish.objects.filter(name=reqPOST['wish_name'])
        if len(wishes_with_same_name) > 0:
            errors['duplicate'] = "That name is already taken!"
        return errors

class Wish(models.Model):
    name = models.CharField(max_length=40)
    description = models.TextField()
    user = models.ForeignKey(User, related_name="wishes_owned", on_delete=models.CASCADE)
    granted_wishes = models.ManyToManyField(User, related_name="wishes_granted")
    likes = models.ManyToManyField(User, related_name="likes")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = WishManager()