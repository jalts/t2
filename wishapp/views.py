from django.shortcuts import render, HttpResponse, redirect
from django.contrib import messages
from .models import *
import bcrypt

def index(request):
    # return HttpResponse("you did")
    return render(request, "index.html")

def register(request):
    if request.method == "POST":
        errors = User.objects.create_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            hashed_pw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt()).decode()
            print(hashed_pw)
            user = User.objects.create(first_name=request.POST['first_name'], last_name=request.POST['last_name'], email=request.POST['email'], password=hashed_pw)
            request.session['user_id'] = user.id
            return redirect('/success')
    return redirect('/')

def login(request):
    user = User.objects.filter(email=request.POST['email'])
    if len(user) > 0:
        user = user[0]
        if bcrypt.checkpw(request.POST['password'].encode(), user.password.encode()):
            request.session['user_id'] = user.id
            return redirect('/success')
    messages.error(request, "email or pass is incorrect")
    return redirect('/')

def success(request):
    if 'user_id' not in request.session:
        messages.error(request, "you need to register or login!")
        return redirect('/')
    user = User.objects.get(id=request.session['user_id']) 
    context = {
        'user': User.objects.get(id=request.session['user_id']),
        'not_my_wishes': Wish.objects.exclude(granted_wishes=user), 
        'my_wishes': Wish.objects.filter(granted_wishes=user),
        'all_wishes': Wish.objects.all(),
    }
    return render(request, "success.html", context)

def create_wish(request):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "POST":
        errors = Wish.objects.create_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/success')
        else:
            wish = Wish.objects.create(name=request.POST['wish_name'], description=request.POST['description'], user=User.objects.get(id=request.session['user_id']))
        return redirect('/success')
    return redirect('/success')

def remove_wish(request, id):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "POST":
        wish_with_id = Wish.objects.filter(id=id)
        if len(wish_with_id) > 0:
            wish = wish_with_id[0]
            if wish.user.id == request.session['user_id']:
                wish.delete()
    return redirect('/success')

def grant_wish(request, id):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "POST":
        wish_with_id = Wish.objects.filter(id=id)
        if len(wish_with_id) > 0:
            wish = Wish.objects.get(id=id)
            user = User.objects.get(id=request.session['user_id'])
            wish.granted_wishes.add(user)
            #user.wishes_granted.add(wish)
    return redirect('/success')

def create_a_wish(request):
    if 'user_id' not in request.session:
        return redirect('/')
    context = {
        "all_wishes": Wish.objects.all(),
        'user': User.objects.get(id=request.session['user_id'])
    }
    return render(request, "make_a_wish.html", context)

def edit_wish(request, id):
    if 'user_id' not in request.session:
        return redirect('/')
    context = {
        'user': User.objects.get(id=request.session['user_id']),
        'wish': Wish.objects.get(id=id),
    }
    return render(request, "edit_wish.html", context)

def update_wish(request, id):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "POST":
        errors = Wish.objects.create_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/success')
        else:
            wish = Wish.objects.get(id=id)
            wish.name = request.POST['wish_name']
            wish.description = request.POST['description']
            wish.save()
        return redirect('/success') 
    return redirect('/success')     

def add_like(request, id):
    liked_wish = Wish.objects.get(id=id)
    user_liking = User.objects.get(id=request.session['user_id'])
    liked_wish.likes.add(user_liking)
    return redirect('/success')







def logout(request):
    request.session.clear()
    return redirect('/')

